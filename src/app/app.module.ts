import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http'
import { InAppBrowser} from '@ionic-native/in-app-browser';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { SearchPage } from '../pages/search/search';
import { ArtistPage } from '../pages/artist/artist';
import { PlaylistPage } from '../pages/playlist/playlist';
import { RelatedartistPage } from '../pages/relatedartist/relatedartist';
import { DetailPage } from '../pages/detail/detail';
import { DetailPlaylistPage } from '../pages/detail-playlist/detail-playlist';
import { IntroPage } from '../pages/intro/intro';
import { LikePage } from '../pages/like/like';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppDataProvider } from '../providers/app-data/app-data';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    ProfilePage,
    SearchPage,
    ArtistPage,
    PlaylistPage,
    RelatedartistPage,
    DetailPage,
    DetailPlaylistPage,
    IntroPage,
    LikePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    ProfilePage,
    SearchPage,
    ArtistPage,
    PlaylistPage,
    RelatedartistPage,
    DetailPage,
    DetailPlaylistPage,
    IntroPage,
    LikePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AppDataProvider,
    InAppBrowser,

  ]
})
export class AppModule {}
